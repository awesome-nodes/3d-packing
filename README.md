# 3D Packing
This library includes an implementation of the __EB-AFIT__ packing algorithm (named in honor to its author 
and the institution where he performed his research) and can be used to find 3D 
packing solutions. The algorithm was originally developed by __Erhan Baltacıoğlu__ at the __U.S. Air Force Institute of 
Technology__ in 2001 (see [The Distributor's Three-Dimensional Pallet-Packing Problem: A Humen Intelligence-Based 
Heuristic Approach](https://apps.dtic.mil/dtic/tr/fulltext/u2/a391201.pdf),
by Erhan Baltacıoğlu, James T. Moore and Raymond R. Hill Jr.).

# Usage
1. Define a __list of container__ objects where items should be packed into:

```ts
import { Container } from '3d-packing/model/container'


const containers = [
    new Container(length, width, height)
];
```

2. Define the __list of items__ that should be packed:

```ts
import { Item } from '3d-packing/model/item'


const items = [
    new Item(length, width, height, quantity)
];
```

3. Define a __list of algorithms__ to use for the packing optimization as specified in the `PackingAlgorithmType` - Enum:

```ts
import { PackingAlgorithmTypes } from '3d-packing/model/core/PackingAlgorithmTypes';


const algorithms = [
    PackingAlgorithmTypes.EB_AFIT
];
```

4. Get an instance of a __Packing Service__ to call its __Pack__ method with all your
   __containers__, __items__ and __algorithms__:

```ts
import { PackingService } from '3d-packing/service/PackingService';


const service        = new PackingService();
const packingResults = service.Pack(containers, items, algorithms);
```

The `PackagingService.Pack(...)` method will try to pack __all the containers__ with __all the items__ using __all the
requested algorithms__.

# Packing Result
For each given container the result of the `PackingService.Pack(...)` method contains one `ContainerPackingResult`
that itself holds an `AlgorithmPackingResult` - object for each `PackaingAlgorithmType` that has been passed.

## Algorithm Packing Result
The algorithm packing result will hold the actual data in respect to the applied inputs. It holds
- Name + Id of the algorithm used
- list of __successfully packed items__
    - in pack order,
    - __location__ of the packed item
    - __orientation__ of the packed item within the container
- list of __items that could not be packed__
- Percentage of the containers' volume that is used
- Percentage of all the items' volumes that could be placed into the container

# Acknowledgements
This project builds upon researches about the EB-AFIT algorithm itself and would not have been possible without the
help of several implementations that lean on the original C code included in Erhan Baltacıoğlu's thesis and
resurrected by Bill Knechtels'
[C-Code Reference implementation of the EB-AFIT algorithm](https://github.com/wknechtel/3d-bin-pack/).
