import { IContainer } from '3d-packing/model/container/IContainer';
import { v4 as NewGuid } from 'uuid';


export class Container implements IContainer
{
    public Id: string;
    public Length: number;
    public Width: number;
    public Height: number;
    public Volume: number;

    public constructor(length: number, width: number, height: number, id: string = NewGuid())
    {
        this.Id = id;
        this.Length = length;
        this.Width = width;
        this.Height = height;
        this.Volume = length * width * height;
    }
}
