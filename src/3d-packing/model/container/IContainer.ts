export interface IContainer
{
    /**
     * Gets or sets the container ID.
     */
    Id: string;

    /**
     * The container ID.
     */
    Length: number;

    /**
     * Gets or sets the container length.
     */
    Width: number;

    /**
     * The container length.
     */
    Height: number;

    /**
     * Gets or sets the container width.
     */
    Volume: number;
}
