import { IAlgorithmPackingResult } from '3d-packing/model/result/IAlgorithmPackingResult';


export interface IContainerPackingResult
{
    /**
     * Gets or sets the container ID.
     */
    ContainerId: string;

    AlgorithmPackingResults: IAlgorithmPackingResult[];
}
