import { PackingAlgorithmTypes } from '3d-packing/model/core';
import { IItem } from '3d-packing/model/item';
import { IAlgorithmPackingResult } from '3d-packing/model/result/IAlgorithmPackingResult';


export class AlgorithmPackingResult implements IAlgorithmPackingResult
{
    private _AlgorithmId!: PackingAlgorithmTypes;
    public set AlgorithmId(value: PackingAlgorithmTypes)
    {
        this._AlgorithmId = value;
    }

    public get AlgorithmName(): string
    {
        return this.AlgorithmId.Name;
    }

    public IsCompletePack!: boolean;
    public PackTimeInMilliseconds!: number;
    public PackedItems: IItem[];
    public PercentContainerVolumePacked!: number;
    public PercentItemVolumePacked!: number;
    public UnpackedItems: IItem[];

    public constructor()
    {
        this.PackedItems   = [];
        this.UnpackedItems = [];
    }
}
