import { IAlgorithmPackingResult } from '3d-packing/model/result/IAlgorithmPackingResult';
import { IContainerPackingResult } from '3d-packing/model/result/IContainerPackingResult';


export class ContainerPackingResult implements IContainerPackingResult
{
    public AlgorithmPackingResults: IAlgorithmPackingResult[];
    public ContainerId!: string;

    public constructor()
    {
        this.AlgorithmPackingResults = [];
    }
}
