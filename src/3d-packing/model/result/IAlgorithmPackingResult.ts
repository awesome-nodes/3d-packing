import { PackingAlgorithmTypes } from '3d-packing/model/core';
import { IItem } from '3d-packing/model/item';


export interface IAlgorithmPackingResult
{
    AlgorithmId: PackingAlgorithmTypes;
    AlgorithmName: string;

    /**
     * Gets or sets a value indicating whether all of the items are packed in the container.
     * @returns True if all the items are packed in the container; otherwise, false.
     */
    IsCompletePack: boolean;

    /**
     * Gets or sets the list of packed items.
     * The list of packed items.
     */
    PackedItems: IItem[];

    /**
     * Gets or sets the elapsed pack time in milliseconds.
     * The elapsed pack time in milliseconds.
     */
    PackTimeInMilliseconds: number;
    /**
     * Gets or sets the percent of container volume packed.
     * The percent of container volume packed.
     */
    PercentContainerVolumePacked: number;

    /**
     * Gets or sets the percent of item volume packed.
     * The percent of item volume packed.
     */
    PercentItemVolumePacked: number;

    /**
     * Gets or sets the list of unpacked items.
     * The list of unpacked items.
     */
    UnpackedItems: IItem[];
}
