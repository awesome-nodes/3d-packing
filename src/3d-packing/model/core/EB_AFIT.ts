import { IContainer } from '3d-packing/model/container';
import { IPackingAlgorithm } from '3d-packing/model/core/IPackingAlgorithm';
import { PackingAlgorithmTypes } from '3d-packing/model/core/PackingAlgorithmTypes';
import { IItem, Item } from '3d-packing/model/item';
import { NumericExtensions } from '3d-packing/model/numeric';
import {
    AlgorithmPackingResult,
    ContainerPackingResult,
    IAlgorithmPackingResult,
    IContainerPackingResult,
} from '3d-packing/model/result';
import { Nullable } from 'simplytyped';


/**
 * A 3D packing algorithm ported from [Github - 3DContainerPacking](https://github.com/davidmchapman/3DContainerPacking),
 * that uses a novel algorithm developed in a U.S. Air Force master's thesis by Erhan Baltacioglu in 2001.
 */
// tslint:disable-next-line:class-name
export class EB_AFIT implements IPackingAlgorithm
{
    // <editor-fold desc="Private member">
    private itemsToPack!: IItem[];
    private itemsPackedInOrder!: IItem[];
    private layers!: Layer[];
    private result!: IContainerPackingResult;

    private scrapfirst!: ScrapPad;
    private smallestZ!: ScrapPad;
    private trash!: ScrapPad;

    private evened!: boolean;
    private hundredPercentPacked = false;
    private layerDone!: boolean;
    private packing!: boolean;
    private packingBest          = false;
    private quit                 = false;

    private bboxi!: number;
    private bestIteration!: number;
    private bestVariant!: number;
    private boxi!: number;
    private cboxi!: number;
    private layerListLen!: number;
    private packedItemCount!: number;
    private x!: number;

    private bbfx!: number;
    private bbfy!: number;
    private bbfz!: number;
    private bboxx!: number;
    private bboxy!: number;
    private bboxz!: number;
    private bfx!: number;
    private bfy!: number;
    private bfz!: number;
    private boxx!: number;
    private boxy!: number;
    private boxz!: number;
    private cboxx!: number;
    private cboxy!: number;
    private cboxz!: number;
    private layerInLayer!: number;
    private layerThickness!: number;
    private lilz!: number;
    private packedVolume!: number;
    private packedy!: number;
    private prelayer!: number;
    private prepackedy!: number;
    private preremainpy!: number;
    private px!: number;
    private py!: number;
    private pz!: number;
    private remainpy!: number;
    private remainpz!: number;
    private itemsToPackCount!: number;
    private totalItemVolume!: number;
    private totalContainerVolume!: number;

    // </editor-fold>

    /**
     * Initializes everything.
     * @param {IContainer} container
     * @param {IItem[]} items
     * @constructor
     * @private
     */
    private Initialize(container: IContainer, items: IItem[]): void
    {
        this.itemsToPack        = [];
        this.itemsPackedInOrder = [];
        this.result             = new ContainerPackingResult();

        // The original code uses 1-based indexing everywhere. This fake entry is added to the beginning
        // of the list to make that possible.
        this.itemsToPack.push(new Item(0, 0, 0, 0));

        this.layers           = [];
        this.itemsToPackCount = 0;

        items.forEach(item =>
        {
            for (let i = 1; i <= item.Quantity; i++) {
                const newItem = new Item(
                    item.Dim1,
                    item.Dim2,
                    item.Dim3,
                    item.Quantity,
                    item.Id + (item.Quantity > 1 ? ` (${i})` : ''),
                );
                this.itemsToPack.push(newItem);
            }
            this.itemsToPackCount += item.Quantity;
        });

        this.itemsToPack.push(new Item(0, 0, 0, 0));

        this.totalContainerVolume = container.Length * container.Height * container.Width;
        this.totalItemVolume      = 0;

        for (this.x = 1; this.x <= this.itemsToPackCount; this.x++) {
            this.totalItemVolume = this.totalItemVolume + this.itemsToPack[this.x].Volume;
        }

        this.scrapfirst = new ScrapPad();

        this.scrapfirst.Pre       = null;
        this.scrapfirst.Post      = null;
        this.packingBest          = false;
        this.hundredPercentPacked = false;
        this.quit                 = false;
    }

    /**
     * Analyzes each unpacked item to find the best fitting one to the empty space given.
     */
    private AnalyzeBox(
        hmx: number,
        hy: number,
        hmy: number,
        hz: number,
        hmz: number,
        dim1: number,
        dim2: number,
        dim3: number): void
    {
        if (dim1 <= hmx && dim2 <= hmy && dim3 <= hmz) {
            if (dim2 <= hy) {
                if (hy - dim2 < this.bfy) {
                    this.boxx = dim1;
                    this.boxy = dim2;
                    this.boxz = dim3;
                    this.bfx  = hmx - dim1;
                    this.bfy  = hy - dim2;
                    this.bfz  = Math.abs(hz - dim3);
                    this.boxi = this.x;
                } else if (hy - dim2 == this.bfy && hmx - dim1 < this.bfx) {
                    this.boxx = dim1;
                    this.boxy = dim2;
                    this.boxz = dim3;
                    this.bfx  = hmx - dim1;
                    this.bfy  = hy - dim2;
                    this.bfz  = Math.abs(hz - dim3);
                    this.boxi = this.x;
                } else if (hy - dim2 == this.bfy && hmx - dim1 == this.bfx && Math.abs(hz - dim3) < this.bfz) {
                    this.boxx = dim1;
                    this.boxy = dim2;
                    this.boxz = dim3;
                    this.bfx  = hmx - dim1;
                    this.bfy  = hy - dim2;
                    this.bfz  = Math.abs(hz - dim3);
                    this.boxi = this.x;
                }
            } else {
                if (dim2 - hy < this.bbfy) {
                    this.bboxx = dim1;
                    this.bboxy = dim2;
                    this.bboxz = dim3;
                    this.bbfx  = hmx - dim1;
                    this.bbfy  = dim2 - hy;
                    this.bbfz  = Math.abs(hz - dim3);
                    this.bboxi = this.x;
                } else if (dim2 - hy == this.bbfy && hmx - dim1 < this.bbfx) {
                    this.bboxx = dim1;
                    this.bboxy = dim2;
                    this.bboxz = dim3;
                    this.bbfx  = hmx - dim1;
                    this.bbfy  = dim2 - hy;
                    this.bbfz  = Math.abs(hz - dim3);
                    this.bboxi = this.x;
                } else if (dim2 - hy == this.bbfy && hmx - dim1 == this.bbfx && Math.abs(hz - dim3) < this.bbfz) {
                    this.bboxx = dim1;
                    this.bboxy = dim2;
                    this.bboxz = dim3;
                    this.bbfx  = hmx - dim1;
                    this.bbfy  = dim2 - hy;
                    this.bbfz  = Math.abs(hz - dim3);
                    this.bboxi = this.x;
                }
            }
        }
    }

    /**
     * After finding each box, the candidate boxes and the condition of the layer are examined.
     */
    private CheckFound(): void
    {
        this.evened = false;

        if (this.boxi != 0) {
            this.cboxi = this.boxi;
            this.cboxx = this.boxx;
            this.cboxy = this.boxy;
            this.cboxz = this.boxz;
        } else {
            if ((this.bboxi > 0) && (this.layerInLayer != 0 || (this.smallestZ.Pre == null && this.smallestZ.Post == null))) {
                if (this.layerInLayer == 0) {
                    this.prelayer = this.layerThickness;
                    this.lilz     = this.smallestZ.CumZ;
                }

                this.cboxi          = this.bboxi;
                this.cboxx          = this.bboxx;
                this.cboxy          = this.bboxy;
                this.cboxz          = this.bboxz;
                this.layerInLayer   = this.layerInLayer + this.bboxy - this.layerThickness;
                this.layerThickness = this.bboxy;
            } else {
                if (this.smallestZ.Pre == null && this.smallestZ.Post == null) {
                    this.layerDone = true;
                } else {
                    this.evened = true;

                    if (this.smallestZ.Pre == null) {
                        this.trash          = this.smallestZ.Post as ScrapPad;
                        this.smallestZ.CumX = this.smallestZ.Post!.CumX;
                        this.smallestZ.CumZ = this.smallestZ.Post!.CumZ;
                        this.smallestZ.Post = this.smallestZ.Post!.Post;
                        if (this.smallestZ.Post != null) {
                            this.smallestZ.Post.Pre = this.smallestZ;
                        }
                    } else if (this.smallestZ.Post == null) {
                        this.smallestZ.Pre.Post = null;
                        this.smallestZ.Pre.CumX = this.smallestZ.CumX;
                    } else {
                        if (this.smallestZ.Pre.CumZ == this.smallestZ.Post.CumZ) {
                            this.smallestZ.Pre.Post = this.smallestZ.Post.Post;

                            if (this.smallestZ.Post.Post != null) {
                                this.smallestZ.Post.Post.Pre = this.smallestZ.Pre;
                            }

                            this.smallestZ.Pre.CumX = this.smallestZ.Post.CumX;
                        } else {
                            this.smallestZ.Pre.Post = this.smallestZ.Post;
                            this.smallestZ.Post.Pre = this.smallestZ.Pre;

                            if (this.smallestZ.Pre.CumZ < this.smallestZ.Post.CumZ) {
                                this.smallestZ.Pre.CumX = this.smallestZ.CumX;
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Executes the packing algorithm variants.
     * @param {IContainer} container
     * @constructor
     * @private
     */
    private ExecuteIterations(container: IContainer): void
    {
        let itelayer: number;
        let layersIndex: number;
        let bestVolume = 0;

        for (let containerOrientationVariant = 1; (containerOrientationVariant <= 6) && !this.quit; containerOrientationVariant++) {
            switch (containerOrientationVariant) {
                case 1:
                    this.px = container.Length;
                    this.py = container.Height;
                    this.pz = container.Width;
                    break;

                case 2:
                    this.px = container.Width;
                    this.py = container.Height;
                    this.pz = container.Length;
                    break;

                case 3:
                    this.px = container.Width;
                    this.py = container.Length;
                    this.pz = container.Height;
                    break;

                case 4:
                    this.px = container.Height;
                    this.py = container.Length;
                    this.pz = container.Width;
                    break;

                case 5:
                    this.px = container.Length;
                    this.py = container.Width;
                    this.pz = container.Height;
                    break;

                case 6:
                    this.px = container.Height;
                    this.py = container.Width;
                    this.pz = container.Length;
                    break;
            }

            this.layers.push(new Layer(null, -1));
            this.ListCanditLayers();
            this.layers.sort((a, b) => NumericExtensions.Compare.Ascending(a.LayerEval as number, b.LayerEval as number));

            for (layersIndex = 1; (layersIndex <= this.layerListLen) && !this.quit; layersIndex++) {
                this.packedVolume    = 0;
                this.packedy         = 0;
                this.packing         = true;
                this.layerThickness  = this.layers[layersIndex].LayerDim as number;
                itelayer             = layersIndex;
                this.remainpy        = this.py;
                this.remainpz        = this.pz;
                this.packedItemCount = 0;

                for (this.x = 1; this.x <= this.itemsToPackCount; this.x++) {
                    this.itemsToPack[this.x].IsPacked = false;
                }

                do {
                    this.layerInLayer = 0;
                    this.layerDone    = false;

                    this.PackLayer();

                    this.packedy  = this.packedy + this.layerThickness;
                    this.remainpy = this.py - this.packedy;

                    if (this.layerInLayer != 0 && !this.quit) {
                        this.prepackedy     = this.packedy;
                        this.preremainpy    = this.remainpy;
                        this.remainpy       = this.layerThickness - this.prelayer;
                        this.packedy        = this.packedy - this.layerThickness + this.prelayer;
                        this.remainpz       = this.lilz;
                        this.layerThickness = this.layerInLayer;
                        this.layerDone      = false;

                        this.PackLayer();

                        this.packedy  = this.prepackedy;
                        this.remainpy = this.preremainpy;
                        this.remainpz = this.pz;
                    }

                    this.FindLayer(this.remainpy);
                } while (this.packing && !this.quit);

                if ((this.packedVolume > bestVolume) && !this.quit) {
                    bestVolume         = this.packedVolume;
                    this.bestVariant   = containerOrientationVariant;
                    this.bestIteration = itelayer;
                }

                if (this.hundredPercentPacked) break;
            }

            if (this.hundredPercentPacked) break;

            if ((container.Length == container.Height) && (container.Height == container.Width)) containerOrientationVariant = 6;

            this.layers = [];
        }
    }

    /**
     * Finds the most proper boxes by looking at all six possible orientations,
     * empty space given, adjacent boxes, and pallet limits.
     * @param {number} hmx
     * @param {number} hy
     * @param {number} hmy
     * @param {number} hz
     * @param {number} hmz
     * @constructor
     * @private
     */
    private FindBox(hmx: number, hy: number, hmy: number, hz: number, hmz: number): void
    {
        let y: number;
        this.bfx   = 32767;
        this.bfy   = 32767;
        this.bfz   = 32767;
        this.bbfx  = 32767;
        this.bbfy  = 32767;
        this.bbfz  = 32767;
        this.boxi  = 0;
        this.bboxi = 0;

        for (y = 1; y <= this.itemsToPackCount; y = y + this.itemsToPack[y].Quantity) {
            for (this.x = y; this.x < this.x + this.itemsToPack[y].Quantity - 1; this.x++) {
                if (!this.itemsToPack[this.x].IsPacked) break;
            }

            if (this.itemsToPack[this.x].IsPacked) continue;

            if (this.x > this.itemsToPackCount) return;

            this.AnalyzeBox(
                hmx,
                hy,
                hmy,
                hz,
                hmz,
                this.itemsToPack[this.x].Dim1,
                this.itemsToPack[this.x].Dim2,
                this.itemsToPack[this.x].Dim3,
            );

            if ((this.itemsToPack[this.x].Dim1 == this.itemsToPack[this.x].Dim3)
                && (this.itemsToPack[this.x].Dim3 == this.itemsToPack[this.x].Dim2)) continue;

            this.AnalyzeBox(
                hmx,
                hy,
                hmy,
                hz,
                hmz,
                this.itemsToPack[this.x].Dim1,
                this.itemsToPack[this.x].Dim3,
                this.itemsToPack[this.x].Dim2,
            );
            this.AnalyzeBox(
                hmx,
                hy,
                hmy,
                hz,
                hmz,
                this.itemsToPack[this.x].Dim2,
                this.itemsToPack[this.x].Dim1,
                this.itemsToPack[this.x].Dim3,
            );
            this.AnalyzeBox(
                hmx,
                hy,
                hmy,
                hz,
                hmz,
                this.itemsToPack[this.x].Dim2,
                this.itemsToPack[this.x].Dim3,
                this.itemsToPack[this.x].Dim1,
            );
            this.AnalyzeBox(
                hmx,
                hy,
                hmy,
                hz,
                hmz,
                this.itemsToPack[this.x].Dim3,
                this.itemsToPack[this.x].Dim1,
                this.itemsToPack[this.x].Dim2,
            );
            this.AnalyzeBox(
                hmx,
                hy,
                hmy,
                hz,
                hmz,
                this.itemsToPack[this.x].Dim3,
                this.itemsToPack[this.x].Dim2,
                this.itemsToPack[this.x].Dim1,
            );
        }
    }

    /**
     * Finds the most proper layer height by looking at the unpacked boxes and the remaining empty space available.
     * @param {number} thickness
     * @constructor
     * @private
     */
    private FindLayer(thickness: number): void
    {
        let exdim           = 0;
        let dimdif: number;
        let dimen2          = 0;
        let dimen3          = 0;
        let y: number;
        let z: number;
        let layereval: number;
        let _eval: number;
        this.layerThickness = 0;
        _eval               = 1000000;

        for (this.x = 1; this.x <= this.itemsToPackCount; this.x++) {
            if (this.itemsToPack[this.x].IsPacked) continue;

            for (y = 1; y <= 3; y++) {
                switch (y) {
                    case 1:
                        exdim  = this.itemsToPack[this.x].Dim1;
                        dimen2 = this.itemsToPack[this.x].Dim2;
                        dimen3 = this.itemsToPack[this.x].Dim3;
                        break;

                    case 2:
                        exdim  = this.itemsToPack[this.x].Dim2;
                        dimen2 = this.itemsToPack[this.x].Dim1;
                        dimen3 = this.itemsToPack[this.x].Dim3;
                        break;

                    case 3:
                        exdim  = this.itemsToPack[this.x].Dim3;
                        dimen2 = this.itemsToPack[this.x].Dim1;
                        dimen3 = this.itemsToPack[this.x].Dim2;
                        break;
                }

                layereval = 0;

                if ((exdim <= thickness) && (
                    ((dimen2 <= this.px) && (dimen3 <= this.pz)) || ((dimen3 <= this.px) && (dimen2 <= this.pz))
                )) {
                    for (z = 1; z <= this.itemsToPackCount; z++) {
                        if (!(this.x == z) && !(this.itemsToPack[z].IsPacked)) {
                            dimdif = Math.abs(exdim - this.itemsToPack[z].Dim1);

                            if (Math.abs(exdim - this.itemsToPack[z].Dim2) < dimdif) {
                                dimdif = Math.abs(exdim - this.itemsToPack[z].Dim2);
                            }

                            if (Math.abs(exdim - this.itemsToPack[z].Dim3) < dimdif) {
                                dimdif = Math.abs(exdim - this.itemsToPack[z].Dim3);
                            }

                            layereval = layereval + dimdif;
                        }
                    }

                    if (layereval < _eval) {
                        _eval               = layereval;
                        this.layerThickness = exdim;
                    }
                }
            }
        }

        if (this.layerThickness == 0 || this.layerThickness > this.remainpy) this.packing = false;
    }

    /**
     * Finds the first to be packed gap in the layer edge.
     * @constructor
     * @private
     */
    private FindSmallestZ(): void
    {
        let scrapmemb: ScrapPad = this.scrapfirst;
        this.smallestZ          = scrapmemb;

        while (scrapmemb.Post != null) {
            if (scrapmemb.Post.CumZ < this.smallestZ.CumZ) {
                this.smallestZ = scrapmemb.Post;
            }

            scrapmemb = scrapmemb.Post;
        }
    }

    /**
     * Lists all possible layer heights by giving a weight value to each of them.
     * @constructor
     * @private
     */
    private ListCanditLayers(): void
    {
        let same: boolean;
        let exdim  = 0;
        let dimdif: number;
        let dimen2 = 0;
        let dimen3 = 0;
        let y: number;
        let z: number;
        let k: number;
        let layereval: number;

        this.layerListLen = 0;

        for (this.x = 1; this.x <= this.itemsToPackCount; this.x++) {
            for (y = 1; y <= 3; y++) {
                switch (y) {
                    case 1:
                        exdim  = this.itemsToPack[this.x].Dim1;
                        dimen2 = this.itemsToPack[this.x].Dim2;
                        dimen3 = this.itemsToPack[this.x].Dim3;
                        break;

                    case 2:
                        exdim  = this.itemsToPack[this.x].Dim2;
                        dimen2 = this.itemsToPack[this.x].Dim1;
                        dimen3 = this.itemsToPack[this.x].Dim3;
                        break;

                    case 3:
                        exdim  = this.itemsToPack[this.x].Dim3;
                        dimen2 = this.itemsToPack[this.x].Dim1;
                        dimen3 = this.itemsToPack[this.x].Dim2;
                        break;
                }

                if ((exdim > this.py)
                    || (((dimen2 > this.px) || (dimen3 > this.pz)) && ((dimen3 > this.px) || (dimen2 > this.pz)))
                ) continue;

                same = false;

                for (k = 1; k <= this.layerListLen; k++) {
                    if (exdim == this.layers[k].LayerDim) {
                        same = true;

                    }
                }

                if (same) continue;

                layereval = 0;

                for (z = 1; z <= this.itemsToPackCount; z++) {
                    if (!(this.x == z)) {
                        dimdif = Math.abs(exdim - this.itemsToPack[z].Dim1);

                        if (Math.abs(exdim - this.itemsToPack[z].Dim2) < dimdif) {
                            dimdif = Math.abs(exdim - this.itemsToPack[z].Dim2);
                        }
                        if (Math.abs(exdim - this.itemsToPack[z].Dim3) < dimdif) {
                            dimdif = Math.abs(exdim - this.itemsToPack[z].Dim3);
                        }
                        layereval = layereval + dimdif;
                    }
                }

                this.layerListLen++;

                this.layers.push(new Layer(null, null));
                this.layers[this.layerListLen].LayerEval = layereval;
                this.layers[this.layerListLen].LayerDim  = exdim;
            }
        }
    }

    /**
     * Transforms the found coordinate system to the one entered by the user and writes them
     * to the report file.
     * @constructor
     * @private
     */
    private OutputBoxList(): void
    {
        let packCoordX = 0;
        let packCoordY = 0;
        let packCoordZ = 0;
        let packDimX   = 0;
        let packDimY   = 0;
        let packDimZ   = 0;

        switch (this.bestVariant) {
            case 1:
                packCoordX = this.itemsToPack[this.cboxi].CoordX;
                packCoordY = this.itemsToPack[this.cboxi].CoordY;
                packCoordZ = this.itemsToPack[this.cboxi].CoordZ;
                packDimX   = this.itemsToPack[this.cboxi].PackDimX;
                packDimY   = this.itemsToPack[this.cboxi].PackDimY;
                packDimZ   = this.itemsToPack[this.cboxi].PackDimZ;
                break;

            case 2:
                packCoordX = this.itemsToPack[this.cboxi].CoordZ;
                packCoordY = this.itemsToPack[this.cboxi].CoordY;
                packCoordZ = this.itemsToPack[this.cboxi].CoordX;
                packDimX   = this.itemsToPack[this.cboxi].PackDimZ;
                packDimY   = this.itemsToPack[this.cboxi].PackDimY;
                packDimZ   = this.itemsToPack[this.cboxi].PackDimX;
                break;

            case 3:
                packCoordX = this.itemsToPack[this.cboxi].CoordY;
                packCoordY = this.itemsToPack[this.cboxi].CoordZ;
                packCoordZ = this.itemsToPack[this.cboxi].CoordX;
                packDimX   = this.itemsToPack[this.cboxi].PackDimY;
                packDimY   = this.itemsToPack[this.cboxi].PackDimZ;
                packDimZ   = this.itemsToPack[this.cboxi].PackDimX;
                break;

            case 4:
                packCoordX = this.itemsToPack[this.cboxi].CoordY;
                packCoordY = this.itemsToPack[this.cboxi].CoordX;
                packCoordZ = this.itemsToPack[this.cboxi].CoordZ;
                packDimX   = this.itemsToPack[this.cboxi].PackDimY;
                packDimY   = this.itemsToPack[this.cboxi].PackDimX;
                packDimZ   = this.itemsToPack[this.cboxi].PackDimZ;
                break;

            case 5:
                packCoordX = this.itemsToPack[this.cboxi].CoordX;
                packCoordY = this.itemsToPack[this.cboxi].CoordZ;
                packCoordZ = this.itemsToPack[this.cboxi].CoordY;
                packDimX   = this.itemsToPack[this.cboxi].PackDimX;
                packDimY   = this.itemsToPack[this.cboxi].PackDimZ;
                packDimZ   = this.itemsToPack[this.cboxi].PackDimY;
                break;

            case 6:
                packCoordX = this.itemsToPack[this.cboxi].CoordZ;
                packCoordY = this.itemsToPack[this.cboxi].CoordX;
                packCoordZ = this.itemsToPack[this.cboxi].CoordY;
                packDimX   = this.itemsToPack[this.cboxi].PackDimZ;
                packDimY   = this.itemsToPack[this.cboxi].PackDimX;
                packDimZ   = this.itemsToPack[this.cboxi].PackDimY;
                break;
        }

        this.itemsToPack[this.cboxi].CoordX   = packCoordX;
        this.itemsToPack[this.cboxi].CoordY   = packCoordY;
        this.itemsToPack[this.cboxi].CoordZ   = packCoordZ;
        this.itemsToPack[this.cboxi].PackDimX = packDimX;
        this.itemsToPack[this.cboxi].PackDimY = packDimY;
        this.itemsToPack[this.cboxi].PackDimZ = packDimZ;

        this.itemsPackedInOrder.push(this.itemsToPack[this.cboxi]);
    }

    /**
     * Packs the boxes found and arranges all variables and records properly.
     * @constructor
     * @private
     */
    private PackLayer(): void
    {
        let lenx: number;
        let lenz: number;
        let lpz: number;

        if (this.layerThickness == 0) {
            this.packing = false;
            return;
        }

        this.scrapfirst.CumX = this.px;
        this.scrapfirst.CumZ = 0;

        for (; !this.quit;) {
            this.FindSmallestZ();

            if ((this.smallestZ.Pre == null) && (this.smallestZ.Post == null)) {
                // *** SITUATION-1: NO BOXES ON THE RIGHT AND LEFT SIDES ***

                lenx = this.smallestZ.CumX;
                lpz  = this.remainpz - this.smallestZ.CumZ;
                this.FindBox(lenx, this.layerThickness, this.remainpy, lpz, lpz);
                this.CheckFound();

                if (this.layerDone) break;
                if (this.evened) continue;

                this.itemsToPack[this.cboxi].CoordX = 0;
                this.itemsToPack[this.cboxi].CoordY = this.packedy;
                this.itemsToPack[this.cboxi].CoordZ = this.smallestZ.CumZ;
                if (this.cboxx == this.smallestZ.CumX) {
                    this.smallestZ.CumZ = this.smallestZ.CumZ + this.cboxz;
                } else {
                    this.smallestZ.Post      = new ScrapPad();
                    this.smallestZ.Post.Post = null;
                    this.smallestZ.Post.Pre  = this.smallestZ;
                    this.smallestZ.Post.CumX = this.smallestZ.CumX;
                    this.smallestZ.Post.CumZ = this.smallestZ.CumZ;
                    this.smallestZ.CumX      = this.cboxx;
                    this.smallestZ.CumZ      = this.smallestZ.CumZ + this.cboxz;
                }
            } else if (this.smallestZ.Pre == null) {
                // *** SITUATION-2: NO BOXES ON THE LEFT SIDE ***

                lenx = this.smallestZ.CumX;
                lenz = this.smallestZ.Post!.CumZ - this.smallestZ.CumZ;
                lpz  = this.remainpz - this.smallestZ.CumZ;
                this.FindBox(lenx, this.layerThickness, this.remainpy, lenz, lpz);
                this.CheckFound();

                if (this.layerDone) break;
                if (this.evened) continue;

                this.itemsToPack[this.cboxi].CoordY = this.packedy;
                this.itemsToPack[this.cboxi].CoordZ = this.smallestZ.CumZ;
                if (this.cboxx == this.smallestZ.CumX) {
                    this.itemsToPack[this.cboxi].CoordX = 0;

                    if (this.smallestZ.CumZ + this.cboxz == this.smallestZ.Post!.CumZ) {
                        this.smallestZ.CumZ = this.smallestZ.Post!.CumZ;
                        this.smallestZ.CumX = this.smallestZ.Post!.CumX;
                        this.trash          = this.smallestZ.Post as ScrapPad;
                        this.smallestZ.Post = this.smallestZ.Post!.Post;

                        if (this.smallestZ.Post != null) {
                            this.smallestZ.Post.Pre = this.smallestZ;
                        }
                    } else {
                        this.smallestZ.CumZ = this.smallestZ.CumZ + this.cboxz;
                    }
                } else {
                    this.itemsToPack[this.cboxi].CoordX = this.smallestZ.CumX - this.cboxx;

                    if (this.smallestZ.CumZ + this.cboxz == this.smallestZ.Post!.CumZ) {
                        this.smallestZ.CumX = this.smallestZ.CumX - this.cboxx;
                    } else {
                        this.smallestZ.Post!.Pre      = new ScrapPad();
                        this.smallestZ.Post!.Pre.Post = this.smallestZ.Post;
                        this.smallestZ.Post!.Pre.Pre  = this.smallestZ;
                        this.smallestZ.Post           = this.smallestZ.Post!.Pre;
                        this.smallestZ.Post!.CumX     = this.smallestZ.CumX;
                        this.smallestZ.CumX           = this.smallestZ.CumX - this.cboxx;
                        this.smallestZ.Post!.CumZ     = this.smallestZ.CumZ + this.cboxz;
                    }
                }
            } else if (this.smallestZ.Post == null) {
                // *** SITUATION-3: NO BOXES ON THE RIGHT SIDE ***
                lenx = this.smallestZ.CumX - this.smallestZ.Pre.CumX;
                lenz = this.smallestZ.Pre.CumZ - this.smallestZ.CumZ;
                lpz  = this.remainpz - this.smallestZ.CumZ;
                this.FindBox(lenx, this.layerThickness, this.remainpy, lenz, lpz);
                this.CheckFound();

                if (this.layerDone) break;
                if (this.evened) continue;

                this.itemsToPack[this.cboxi].CoordY = this.packedy;
                this.itemsToPack[this.cboxi].CoordZ = this.smallestZ.CumZ;
                this.itemsToPack[this.cboxi].CoordX = this.smallestZ.Pre.CumX;

                if (this.cboxx == this.smallestZ.CumX - this.smallestZ.Pre.CumX) {
                    if (this.smallestZ.CumZ + this.cboxz == this.smallestZ.Pre.CumZ) {
                        this.smallestZ.Pre.CumX = this.smallestZ.CumX;
                        this.smallestZ.Pre.Post = null;
                    } else {
                        this.smallestZ.CumZ = this.smallestZ.CumZ + this.cboxz;
                    }
                } else {
                    if (this.smallestZ.CumZ + this.cboxz == this.smallestZ.Pre.CumZ) {
                        this.smallestZ.Pre.CumX = this.smallestZ.Pre.CumX + this.cboxx;
                    } else {
                        this.smallestZ.Pre.Post      = new ScrapPad();
                        this.smallestZ.Pre.Post.Pre  = this.smallestZ.Pre;
                        this.smallestZ.Pre.Post.Post = this.smallestZ;
                        this.smallestZ.Pre           = this.smallestZ.Pre.Post;
                        this.smallestZ.Pre.CumX      = this.smallestZ.Pre.Pre!.CumX + this.cboxx;
                        this.smallestZ.Pre.CumZ      = this.smallestZ.CumZ + this.cboxz;
                    }
                }
            } else if (this.smallestZ.Pre.CumZ == this.smallestZ.Post.CumZ) {
                // *** SITUATION-4: THERE ARE BOXES ON BOTH OF THE SIDES ***

                // *** SUBSITUATION-4A: SIDES ARE EQUAL TO EACH OTHER ***

                lenx = this.smallestZ.CumX - this.smallestZ.Pre.CumX;
                lenz = this.smallestZ.Pre.CumZ - this.smallestZ.CumZ;
                lpz  = this.remainpz - this.smallestZ.CumZ;

                this.FindBox(lenx, this.layerThickness, this.remainpy, lenz, lpz);
                this.CheckFound();

                if (this.layerDone) break;
                if (this.evened) continue;

                this.itemsToPack[this.cboxi].CoordY = this.packedy;
                this.itemsToPack[this.cboxi].CoordZ = this.smallestZ.CumZ;

                if (this.cboxx == this.smallestZ.CumX - this.smallestZ.Pre.CumX) {
                    this.itemsToPack[this.cboxi].CoordX = this.smallestZ.Pre.CumX;

                    if (this.smallestZ.CumZ + this.cboxz == this.smallestZ.Post.CumZ) {
                        this.smallestZ.Pre.CumX = this.smallestZ.Post.CumX;

                        if (this.smallestZ.Post.Post != null) {
                            this.smallestZ.Pre.Post      = this.smallestZ.Post.Post;
                            this.smallestZ.Post.Post.Pre = this.smallestZ.Pre;
                        } else {
                            this.smallestZ.Pre.Post = null;
                        }
                    } else {
                        this.smallestZ.CumZ = this.smallestZ.CumZ + this.cboxz;
                    }
                } else if (this.smallestZ.Pre.CumX < this.px - this.smallestZ.CumX) {
                    if (this.smallestZ.CumZ + this.cboxz == this.smallestZ.Pre.CumZ) {
                        this.smallestZ.CumX = this.smallestZ.CumX - this.cboxx;
                        // NOTE: As supposed [here](https://github.com/wknechtel/3d-bin-pack/issues/2) the reference
                        // implementation introduced a bug by decrementing the cboxx twice from smallestZ.CumX
                        this.itemsToPack[this.cboxi].CoordX = this.smallestZ.CumX; // - this.cboxx;
                    } else {
                        this.itemsToPack[this.cboxi].CoordX = this.smallestZ.Pre.CumX;
                        this.smallestZ.Pre.Post             = new ScrapPad();
                        this.smallestZ.Pre.Post.Pre         = this.smallestZ.Pre;
                        this.smallestZ.Pre.Post.Post        = this.smallestZ;
                        this.smallestZ.Pre                  = this.smallestZ.Pre.Post;
                        this.smallestZ.Pre.CumX             = this.smallestZ.Pre.Pre!.CumX + this.cboxx;
                        this.smallestZ.Pre.CumZ             = this.smallestZ.CumZ + this.cboxz;
                    }
                } else {
                    if (this.smallestZ.CumZ + this.cboxz == this.smallestZ.Pre.CumZ) {
                        this.smallestZ.Pre.CumX             = this.smallestZ.Pre.CumX + this.cboxx;
                        this.itemsToPack[this.cboxi].CoordX = this.smallestZ.Pre.CumX;
                    } else {
                        this.itemsToPack[this.cboxi].CoordX = this.smallestZ.CumX - this.cboxx;
                        this.smallestZ.Post.Pre             = new ScrapPad();
                        this.smallestZ.Post.Pre.Post        = this.smallestZ.Post;
                        this.smallestZ.Post.Pre.Pre         = this.smallestZ;
                        this.smallestZ.Post                 = this.smallestZ.Post.Pre;
                        this.smallestZ.Post.CumX            = this.smallestZ.CumX;
                        this.smallestZ.Post.CumZ            = this.smallestZ.CumZ + this.cboxz;
                        this.smallestZ.CumX                 = this.smallestZ.CumX - this.cboxx;
                    }
                }
            } else {
                // *** SUBSITUATION-4B: SIDES ARE NOT EQUAL TO EACH OTHER ***

                lenx = this.smallestZ.CumX - this.smallestZ.Pre.CumX;
                lenz = this.smallestZ.Pre.CumZ - this.smallestZ.CumZ;
                lpz  = this.remainpz - this.smallestZ.CumZ;
                this.FindBox(lenx, this.layerThickness, this.remainpy, lenz, lpz);
                this.CheckFound();

                if (this.layerDone) break;
                if (this.evened) continue;

                this.itemsToPack[this.cboxi].CoordY = this.packedy;
                this.itemsToPack[this.cboxi].CoordZ = this.smallestZ.CumZ;
                this.itemsToPack[this.cboxi].CoordX = this.smallestZ.Pre.CumX;

                if (this.cboxx == (this.smallestZ.CumX - this.smallestZ.Pre.CumX)) {
                    if ((this.smallestZ.CumZ + this.cboxz) == this.smallestZ.Pre.CumZ) {
                        this.smallestZ.Pre.CumX = this.smallestZ.CumX;
                        this.smallestZ.Pre.Post = this.smallestZ.Post;
                        this.smallestZ.Post.Pre = this.smallestZ.Pre;
                    } else {
                        this.smallestZ.CumZ = this.smallestZ.CumZ + this.cboxz;
                    }
                } else {
                    if ((this.smallestZ.CumZ + this.cboxz) == this.smallestZ.Pre.CumZ) {
                        this.smallestZ.Pre.CumX = this.smallestZ.Pre.CumX + this.cboxx;
                    } else if (this.smallestZ.CumZ + this.cboxz == this.smallestZ.Post.CumZ) {
                        this.itemsToPack[this.cboxi].CoordX = this.smallestZ.CumX - this.cboxx;
                        this.smallestZ.CumX                 = this.smallestZ.CumX - this.cboxx;
                    } else {
                        this.smallestZ.Pre.Post = new ScrapPad();

                        this.smallestZ.Pre.Post.Pre  = this.smallestZ.Pre;
                        this.smallestZ.Pre.Post.Post = this.smallestZ;
                        this.smallestZ.Pre           = this.smallestZ.Pre.Post;
                        this.smallestZ.Pre.CumX      = this.smallestZ.Pre.Pre!.CumX + this.cboxx;
                        this.smallestZ.Pre.CumZ      = this.smallestZ.CumZ + this.cboxz;
                    }
                }
            }

            this.VolumeCheck();
        }
    }

    /**
     * Using the parameters found, packs the best solution found and
     * reports to the console.
     * @param {IContainer} container
     * @constructor
     * @private
     */
    private Report(container: IContainer): void
    {
        this.quit = false;

        switch (this.bestVariant) {
            case 1:
                this.px = container.Length;
                this.py = container.Height;
                this.pz = container.Width;
                break;

            case 2:
                this.px = container.Width;
                this.py = container.Height;
                this.pz = container.Length;
                break;

            case 3:
                this.px = container.Width;
                this.py = container.Length;
                this.pz = container.Height;
                break;

            case 4:
                this.px = container.Height;
                this.py = container.Length;
                this.pz = container.Width;
                break;

            case 5:
                this.px = container.Length;
                this.py = container.Width;
                this.pz = container.Height;
                break;

            case 6:
                this.px = container.Height;
                this.py = container.Width;
                this.pz = container.Length;
                break;
        }

        this.packingBest = true;

        // Print("BEST SOLUTION FOUND AT ITERATION               :", bestIteration, "OF VARIANT", bestVariant);
        // Print("TOTAL ITEMS TO PACK                            :", itemsToPackCount);
        // Print("TOTAL VOLUME OF ALL ITEMS                      :", totalItemVolume);
        // Print("WHILE CONTAINER ORIENTATION X - Y - Z          :", px, py, pz);

        this.layers = [];
        this.layers.push(new Layer(null, -1));
        this.ListCanditLayers();
        this.layers         = this.layers.sort((a, b) => NumericExtensions.Compare.Ascending(a.LayerEval as number, b.LayerEval as number));
        this.packedVolume   = 0;
        this.packedy        = 0;
        this.packing        = true;
        this.layerThickness = this.layers[this.bestIteration].LayerDim as number;
        this.remainpy       = this.py;
        this.remainpz       = this.pz;

        for (this.x = 1; this.x <= this.itemsToPackCount; this.x++) {
            this.itemsToPack[this.x].IsPacked = false;
        }

        do {
            this.layerInLayer = 0;
            this.layerDone    = false;
            this.PackLayer();
            this.packedy  = this.packedy + this.layerThickness;
            this.remainpy = this.py - this.packedy;

            if (this.layerInLayer > 0.0001) {
                this.prepackedy     = this.packedy;
                this.preremainpy    = this.remainpy;
                this.remainpy       = this.layerThickness - this.prelayer;
                this.packedy        = this.packedy - this.layerThickness + this.prelayer;
                this.remainpz       = this.lilz;
                this.layerThickness = this.layerInLayer;
                this.layerDone      = false;
                this.PackLayer();
                this.packedy  = this.prepackedy;
                this.remainpy = this.preremainpy;
                this.remainpz = this.pz;
            }

            if (!this.quit) {
                this.FindLayer(this.remainpy);
            }
        } while (this.packing && !this.quit);
    }

    /**
     * After packing of each item, the 100% packing condition is checked.
     * @constructor
     * @private
     */
    private VolumeCheck(): void
    {
        this.itemsToPack[this.cboxi].IsPacked = true;
        this.itemsToPack[this.cboxi].PackDimX = this.cboxx;
        this.itemsToPack[this.cboxi].PackDimY = this.cboxy;
        this.itemsToPack[this.cboxi].PackDimZ = this.cboxz;
        this.packedVolume                     = this.packedVolume + this.itemsToPack[this.cboxi].Volume;
        this.packedItemCount++;

        if (this.packingBest) {
            this.OutputBoxList();
        } else if (this.packedVolume == this.totalContainerVolume || this.packedVolume == this.totalItemVolume) {
            this.packing              = false;
            this.hundredPercentPacked = true;
        }
    }

    /**
     * Runs the packing algorithm.
     * @param {IContainer} container The container to pack items into.
     * @param {IItem[]} items The items to pack.
     * @returns {IAlgorithmPackingResult} The packing result.
     */
    public Run(container: IContainer, items: IItem[]): IAlgorithmPackingResult
    {
        this.Initialize(container, items);
        this.ExecuteIterations(container);
        this.Report(container);

        const result       = new AlgorithmPackingResult();
        result.AlgorithmId = PackingAlgorithmTypes.EB_AFIT;

        for (let i = 1; i <= this.itemsToPackCount; i++) {
            this.itemsToPack[i].Quantity = 1;

            if (!this.itemsToPack[i].IsPacked) {
                result.UnpackedItems.push(this.itemsToPack[i]);
            }
        }

        result.PackedItems = this.itemsPackedInOrder;

        if (result.UnpackedItems.length == 0) {
            result.IsCompletePack = true;
        }

        return result;
    }
}

/**
 * A list that stores all the different lengths of all item dimensions.
 * From the master's thesis:
 * "Each Layerdim value in this array represents a different layer thickness
 * value with which each iteration can start packing. Before starting iterations,
 * all different lengths of all box dimensions along with evaluation values are
 * stored in this array" (p. 3-6).
 */
class Layer
{

    /**
     * Gets or sets the layer dimension value, representing a layer thickness.
     * The layer dimension value.
     * @type {number}
     */
    public get LayerDim(): Nullable<number>
    {
        return this._LayerDim;
    }

    public set LayerDim(value: Nullable<number>)
    {
        this._LayerDim = value;
    }

    /**
     * Gets or sets the layer eval value, representing an evaluation weight
     * value for the corresponding LayerDim value.
     * The layer eval value.
     */
    public get LayerEval(): Nullable<number>
    {
        return this._LayerEval;
    }

    public set LayerEval(value: Nullable<number>)
    {
        this._LayerEval = value;
    }

    public constructor(
        private _LayerDim: Nullable<number>,
        private _LayerEval: Nullable<number>)
    {
    }
}

/**
 * From the master's thesis:
 * "The double linked list we use keeps the topology of the edge of the
 * current layer under construction. We keep the x and z coordinates of
 * each gap's right corner. The program looks at those gaps and tries to
 * fill them with boxes one at a time while trying to keep the edge of the
 * layer even" (p. 3-7).
 */
class ScrapPad
{
    /**
     * Gets or sets the x coordinate of the gap's right corner.
     * @type {number}
     */
    public CumX!: number;

    /**
     * Gets or sets the z coordinate of the gap's right corner.
     * @type {number}
     */
    public CumZ!: number;

    /**
     * Gets or sets the following entry.
     * @type {ScrapPad}
     */
    public Post!: Nullable<ScrapPad>;

    /**
     * Gets or sets the previous entry.
     * @type {ScrapPad}
     */
    public Pre!: Nullable<ScrapPad>;
}
