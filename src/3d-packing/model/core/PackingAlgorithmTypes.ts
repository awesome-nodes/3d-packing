import { Enum } from '@awesome-nodes/object';


export class PackingAlgorithmTypes extends Enum<number>
{
    /**
     * The EB-AFIT algorithm based on heuristics.
     *
     * See [Thesis: The distributer's three-dimensional pallet-packing problem: A human intelligence-based heuristic
     * approach](https://apps.dtic.mil/dtic/tr/fulltext/u2/a391201.pdf)
     */
    public static EB_AFIT = PackingAlgorithmTypes.Enum(0x1, 'EB-AFIT');
}
