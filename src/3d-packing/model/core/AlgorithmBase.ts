import { IContainer } from '3d-packing/model/container';
import { IItem } from '3d-packing/model/item';
import { IContainerPackingResult } from '3d-packing/model/result';


export abstract class AlgorithmBase
{
    abstract Run(container: IContainer, items: IItem[]): IContainerPackingResult;
}
