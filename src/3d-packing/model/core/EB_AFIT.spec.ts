import { Container } from '3d-packing/model/container';
import { PackingAlgorithmTypes } from '3d-packing/model/core/PackingAlgorithmTypes';
import { Item } from '3d-packing/model/item';
import { PackingService } from '3d-packing/service';


describe('Packing algorithm', () =>
    it('Should try to pack items using EB-AFIT algorithm', () =>
    {
        const service = new PackingService();

        const items = [
            new Item(100, 50, 10, 1),
            new Item(100, 50, 20, 2),
            new Item(100, 50, 50, 1),
        ];

        const containers = [

            /**
             * Too small for all items. both Item[1] should be packed and the container should be filled by 80%
             */
            new Container(100, 100, 25),

            /**
             * All items fit in perfectly, 100% of the containers volume is used and all items are packed.
             */
            new Container(100, 100, 50),

            /**
             * All items fit into the container that is left with 50% of its volume free.
             */
            new Container(100, 100, 100), // Passt, zu 50% voll
        ];

        const packingResult = service.Pack(containers, items, [PackingAlgorithmTypes.EB_AFIT]);

        expect(packingResult[0].AlgorithmPackingResults[0].PackedItems.length).toBe(2);
        expect(packingResult[0].AlgorithmPackingResults[0].UnpackedItems.length).toBe(2);
        expect(packingResult[0].AlgorithmPackingResults[0].PercentContainerVolumePacked).toBe(80);
        expect(packingResult[0].AlgorithmPackingResults[0].PercentItemVolumePacked).toBe(40);

        expect(packingResult[1].AlgorithmPackingResults[0].PackedItems.length).toBe(4);
        expect(packingResult[1].AlgorithmPackingResults[0].UnpackedItems.length).toBe(0);
        expect(packingResult[1].AlgorithmPackingResults[0].PercentContainerVolumePacked).toBe(100);
        expect(packingResult[1].AlgorithmPackingResults[0].PercentItemVolumePacked).toBe(100);

        expect(packingResult[2].AlgorithmPackingResults[0].PackedItems.length).toBe(4);
        expect(packingResult[2].AlgorithmPackingResults[0].UnpackedItems.length).toBe(0);
        expect(packingResult[2].AlgorithmPackingResults[0].PercentContainerVolumePacked).toBe(50);
        expect(packingResult[2].AlgorithmPackingResults[0].PercentItemVolumePacked).toBe(100);
    }),
);
