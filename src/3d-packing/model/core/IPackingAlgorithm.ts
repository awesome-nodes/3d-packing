import { IContainer } from '3d-packing/model/container';
import { IItem } from '3d-packing/model/item';
import { IAlgorithmPackingResult } from '3d-packing/model/result';


export interface IPackingAlgorithm
{
    /**
     * Runs the algorithm on the specified container and items.
     * @param {IContainer} container The container to pack items into.
     * @param {IItem[]} items The items to pack.
     * @returns {IAlgorithmPackingResult} The algorithm packing result.
     * @constructor
     */
    Run(container: IContainer, items: IItem[]): IAlgorithmPackingResult;
}

