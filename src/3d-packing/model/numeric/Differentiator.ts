import { Difference } from '3d-packing/model/numeric/Difference';
import { ObjectBase } from '@awesome-nodes/object';


/**
 * Abstract Differentiator that describes the difference of two arbitrary objects. Used for two things:
 * 1. A wrapper for 'IsEqual' - checks to either return true or the amount of a scalable difference of two things.
 * 2. A Base class to implement more complex Comparators that are used to query object hierarchies.
 */
export abstract class Differentiator<T1, T2, T3> extends ObjectBase
{
    protected constructor(public readonly Between: T1, public readonly And: T2, name?: string)
    {
        super(name);
    }

    public abstract get Difference(): true | Difference<T1, T2, T3>;
}
