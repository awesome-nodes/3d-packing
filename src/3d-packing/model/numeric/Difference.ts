import { ObjectBase } from '@awesome-nodes/object';


/**
 * Difference of two arbitrary objects. Can be interpreted as a falsy value. A Difference with a value not equal to
 * 0 means _true_.
 *
 * Used for two things:
 * 1. A wrapper for 'IsEqual' - checks to either return true or the amount of a scalable difference of two things.
 * 2. A Base class to implement more complex Comparators that are used to query object hierarchies.
 */
export class Difference<T1 = any, T2 = any, TValue = number> extends ObjectBase
{
    public constructor(
        public readonly Between: T1,
        public readonly And: T2,
        public readonly Value: TValue,
        name?: string)
    {
        super(name);
    }

    public static NoDifference<T1, T2, TValue extends number>(Between: T1, And: T2): Difference<T1, T2, number>
    {
        return new Difference<T1, T2, number>(Between, And, 0);
    }

    public static NoDifferenceWithinTolerance<T1, T2, TValue extends number>(Tolerance: number, Between: T1, And: T2)
    {
        return new Difference<T1, T2, number>(
            Between, And, 0,
            `Difference (${Math.round(Tolerance * 10000) / 100}%, ${Between}, ${And})`,
        );
    }
}

export type EqualOrDifference<T1, T2, TValue> = true | Difference<T1, T2, TValue>;
