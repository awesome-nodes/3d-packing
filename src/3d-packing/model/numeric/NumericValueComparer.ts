import { Difference } from '3d-packing/model/numeric/Difference';
import { Differentiator } from '3d-packing/model/numeric/Differentiator';


/**
 * Factory for equality checks of two numeric values that accepts a failure rate of the given amount (in %, where
 * 0.1 === 10%).
 * @param {number} accuracy
 * @constructor
 */
export class NumericValueComparer extends Differentiator<number, number, number>
{
    public constructor(Between: number, And: number, public readonly Accuracy: number, name?: string)
    {
        super(Between, And, name);
    }

    public get Difference(): Difference<number, number, number>
    {
        const diff = this.And - this.Between;
        const rel  = Math.abs(this.And === 0
                              ? this.Between && this.Between / Number.MIN_VALUE || 0
                              : diff / this.And,
        );
        if (rel <= this.Accuracy)
            return new Difference(this.Between, this.And, 0);
        return new Difference(this.Between, this.And, diff, `Difference of ${Math.round(rel * 10000) * 100}%`);
    }
}
