import { Difference } from '3d-packing/model/numeric/Difference';
import { NumericValueComparer } from '3d-packing/model/numeric/NumericValueComparer';


export abstract class NumericExtensions
{
    private static Descending = (v1: number, v2: number) => v1 === v2 ? 0
                                                                      : v1 > v2 ? -1
                                                                                : v1 < v2 ? 1
                                                                                          : null as never;

    private static Ascending = (v1: number, v2: number) => v1 === v2 ? 0
                                                                     : v1 > v2 ? 1
                                                                               : v1 < v2 ? -1
                                                                                         : null as never;

    public static Compare = {
        Ascending : NumericExtensions.Ascending,
        Descending: NumericExtensions.Descending,
    };

    public static TopHalf<T>(comparator: (el1: T, el2: T) => number, elements: T[]): T[]
    {
        // If previous selectors already reduced the result set to a single vertex just return it
        if (elements.length == 1) return elements;

        const sorted = elements.slice().sort(comparator);
        return sorted.slice(0, sorted.length / 2);
    }

    public static DifferenceWithTolerance(accuracy: number): (
        comparedNumber1: number,
        comparedNumber2: number) => Difference
    {
        return (n1, n2) => new NumericValueComparer(n1, n2, accuracy).Difference;
    }
}
