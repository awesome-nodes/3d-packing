import { IItem } from '3d-packing/model/item/IItem';
import { v4 as NewGuid } from 'uuid';


export class Item implements IItem
{
    public readonly Id: string;
    public readonly Dim1: number;
    public readonly Dim2: number;
    public readonly Dim3: number;
    public readonly Volume: number;

    public Quantity: number;

    public IsPacked!: boolean;
    public CoordX!: number;
    public CoordY!: number;
    public CoordZ!: number;
    public PackDimX!: number;
    public PackDimY!: number;
    public PackDimZ!: number;

    public constructor(dim1: number, dim2: number, dim3: number, quantity: number, id: string = NewGuid())
    {
        this.Id       = id;
        this.Dim1     = dim1;
        this.Dim2     = dim2;
        this.Dim3     = dim3;
        this.Volume   = dim1 * dim2 * dim3;
        this.Quantity = quantity;
    }
}
