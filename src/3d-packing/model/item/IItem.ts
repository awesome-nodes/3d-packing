export interface IItem
{
    /**
     * Gets or sets the item Id.
     */
    Id: string;

    /**
     * Gets or sets the length of the the first item dimension.
     */
    Dim1: number;

    /**
     * Gets or sets the length of the the second item dimension.
     */
    Dim2: number;

    /**
     * Gets or sets the length of the the third item dimension.
     */
    Dim3: number;

    /**
     * Gets the items volume
     */
    Volume: number;

    /**
     * Gets or sets the item quantity.
     */
    Quantity: number;

    /**
     * Gets or sets a value indicating whether this item has already been packed.
     * True if the item has already been packed; otherwise, false.
     */
    IsPacked: boolean;

    /**
     * Gets or sets the x coordinate of the location of the packed item within the container.
     */
    CoordX: number;

    /**
     * Gets or sets the y coordinate of the location of the packed item within the container.
     */
    CoordY: number;

    /**
     * Gets or sets the z coordinate of the location of the packed item within the container.
     */
    CoordZ: number;


    /**
     * Gets or sets the x dimension of the orientation of the item as it has been packed.
     */
    PackDimX: number;

    /**
     * Gets or sets the y dimension of the orientation of the item as it has been packed.
     */
    PackDimY: number;

    /**
     * Gets or sets the z dimension of the orientation of the item as it has been packed.
     */
    PackDimZ: number;
}
