import { IContainer } from '3d-packing/model/container';
import { IPackingAlgorithm, PackingAlgorithmTypes } from '3d-packing/model/core';
import { IItem } from '3d-packing/model/item';
import { IContainerPackingResult } from '3d-packing/model/result';


export interface IPackingService
{
    Pack(containers: IContainer[], itemsToPack: IItem[], algorithms: PackingAlgorithmTypes[]): IContainerPackingResult[];

    GetPackagingAlgorithmFromTypeId(algorithmType: PackingAlgorithmTypes): IPackingAlgorithm;
}
