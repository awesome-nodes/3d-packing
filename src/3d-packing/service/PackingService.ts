import { IContainer } from '3d-packing/model/container';
import { EB_AFIT, IPackingAlgorithm, PackingAlgorithmTypes } from '3d-packing/model/core';
import { IItem, Item } from '3d-packing/model/item';
import { ContainerPackingResult, IContainerPackingResult } from '3d-packing/model/result';
import { IPackingService } from '3d-packing/service/IPackingService';
import { Exception } from '@awesome-nodes/object';
import { StopWatch } from 'stopwatch-node';


export class PackingService implements IPackingService
{
    public GetPackagingAlgorithmFromTypeId(algorithmType: PackingAlgorithmTypes): IPackingAlgorithm
    {
        switch (algorithmType) {
            case PackingAlgorithmTypes.EB_AFIT:
                return new EB_AFIT();
            default:
                throw new Exception('Invalid algorithm type');
        }
    }

    public Pack(
        containers: IContainer[],
        itemsToPack: IItem[],
        algorithms: PackingAlgorithmTypes[]): IContainerPackingResult[]
    {
        const result: IContainerPackingResult[] = [];
        containers.forEach(container =>
        {
            const containerPackingResult       = new ContainerPackingResult();
            containerPackingResult.ContainerId = container.Id;

            algorithms.forEach(algorithmType =>
            {
                const algorithm = this.GetPackagingAlgorithmFromTypeId(algorithmType);

                const items = new Array<IItem>();

                itemsToPack.forEach(item =>
                {
                    items.push(new Item(item.Dim1, item.Dim2, item.Dim3, item.Quantity, item.Id));
                });

                const stopwatch = new StopWatch(`Packing Service`);
                stopwatch.start(`Pack (containers: ${containers.map(c => c.Id)})`);
                const algorithmResult = algorithm.Run(container, items);
                stopwatch.stop();

                algorithmResult.PackTimeInMilliseconds = stopwatch.getTotalTime();

                const containerVolume    = container.Length * container.Width * container.Height;
                const itemVolumePacked   = algorithmResult.PackedItems.reduce((sum, i) => sum + i.Volume, 0);
                const itemVolumeUnpacked = algorithmResult.UnpackedItems.reduce((sum, i) => sum + i.Volume, 0);

                algorithmResult.PercentContainerVolumePacked =
                    Math.round(itemVolumePacked / containerVolume * 10000) / 100;
                algorithmResult.PercentItemVolumePacked      =
                    Math.round(itemVolumePacked / (itemVolumePacked + itemVolumeUnpacked) * 10000) / 100;

                containerPackingResult.AlgorithmPackingResults.push(algorithmResult);
            });
            containerPackingResult.AlgorithmPackingResults = containerPackingResult.AlgorithmPackingResults.sort(
                (a, b) =>
                    a.AlgorithmName > b.AlgorithmName
                    ? 1
                    : a.AlgorithmName < b.AlgorithmName
                      ? -1
                      : 0,
            );
            result.push(containerPackingResult);
        });
        return result;
    }
}
